package com.processor.microcontroller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.cli.CommandLine;

import com.processor.microcontroller.CPU.CPU;


public class MC8051 {

    private static final Logger LOGGER = Logger.getLogger(MC8051.class.getName());

    public static void main(String[] args) {

        try {
            CommandLine cmd = CommandLineParser.getInstance().parseCommandLineOptions(args);
            Map<String, String> inputs = CommandLineParser.getInstance().getInputsMap(cmd);
            setupLogger(cmd.hasOption(Constants.VERBOSE) ? Level.ALL : Level.INFO);
            LOGGER.log(Level.INFO, "Reading Input Instructions");
            List<Instruction> instructions = InstructionReader.getInstance().readInstructions(inputs.get(Constants.FILEPATH));
            LOGGER.log(Level.INFO, instructions.toString());
            LOGGER.log(Level.INFO, "Generating Opcode");
            List<MachineCode> machineCodes = MachineCodeGenerator.getInstance().generateMachineCode(instructions);
            LOGGER.log(Level.INFO, "Machine Language: {0}", machineCodes);
            initializeOutputFile(inputs.get(Constants.DESTINATION) + Constants.OUTPUT_FILENAME);
            printMachineCode(inputs.get(Constants.DESTINATION) + Constants.OUTPUT_FILENAME, machineCodes);
            LOGGER.log(Level.INFO, "Processing Instructions");
            CPU.getInstance().processInstructions(machineCodes, inputs.get(Constants.DESTINATION) + Constants
                    .OUTPUT_FILENAME);

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (InstructionValidatorException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private static void printMachineCode(String destination, List<MachineCode> machineCodes) throws IOException {
        File file = new File(destination);
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write(" ============================================================\n");
            bufferedWriter.write("|   " + String.format("%-10s", "Address") + "|     " + String.format("%-20s",
                    "Assembly") + "|     " + String.format("%-15s", "Machine Code") + "|\n");
            bufferedWriter.write(" ============================================================\n");

            for (MachineCode machineCode : machineCodes) {
                bufferedWriter.write("|   " + String.format("%-10s", machineCode.getAddress()) + "|     " + String
                        .format("%-20s", machineCode.getAssemblyInstruction().toString()) + "|     " + String.format
                        ("%-15s", machineCode.getMachineLanguage()) + "|\n");
                bufferedWriter.write(" ------------------------------------------------------------\n");
            }

        } finally {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
        }
    }

    private static void initializeOutputFile(String destination) throws IOException {
        File file = new File(destination);
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("        ==========================================\n");
            bufferedWriter.write("       ||     _ _ _     _ _ _    _ _ _    .       ||\n");
            bufferedWriter.write("       ||    |     |   |     |  |         |       ||\n");
            bufferedWriter.write("       ||    |_ _ _|   |     |  |_ _ _    |       ||\n");
            bufferedWriter.write("       ||    |     |   |     |        |   |       ||\n");
            bufferedWriter.write("       ||    |_ _ _|   |_ _ _|   _ _ _|   |       ||\n");
            bufferedWriter.write("       ||                                         ||\n");
            bufferedWriter.write("        ==========================================\n\n");
        } finally {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
        }
    }

    private static void setupLogger(Level level) throws IOException {
        Logger logger = Logger.getLogger(MC8051.class.getPackage().getName());
        new File(Constants.LOG_DIR).mkdir();
        new FileWriter(Constants.FILEHANDLER_LOGPATH).close();
        FileHandler fileHandler = new FileHandler(Constants.FILEHANDLER_LOGPATH);
        fileHandler.setLevel(level);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);
        logger.addHandler(fileHandler);
        logger.setLevel(level);
        logger.setUseParentHandlers(false);
    }
}
