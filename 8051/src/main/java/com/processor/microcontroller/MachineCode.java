package com.processor.microcontroller;

public class MachineCode {
    private String address;
    private String machineLanguage;
    private Instruction assemblyInstruction;

    public MachineCode() {
    }

    public MachineCode(String address, String machineLanguage, Instruction assemblyInstruction) {
        this.address = address;
        this.machineLanguage = machineLanguage;
        this.assemblyInstruction = assemblyInstruction;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *         the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the machineLanguage
     */
    public String getMachineLanguage() {
        return machineLanguage;
    }

    /**
     * @param machineLanguage
     *         the machineLanguage to set
     */
    public void setMachineLanguage(String machineLanguage) {
        this.machineLanguage = machineLanguage;
    }

    /**
     * @return the assemblyInstruction
     */
    public Instruction getAssemblyInstruction() {
        return assemblyInstruction;
    }

    /**
     * @param assemblyInstruction
     *         the assemblyInstruction to set
     */
    public void setAssemblyInstruction(Instruction assemblyInstruction) {
        this.assemblyInstruction = assemblyInstruction;
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\taddress: ").append(address)
                .append("\n\tmachineLanguage: ").append(machineLanguage)
                .append("\n\tassemblyInstruction: ").append(assemblyInstruction)
                .append("\n}");
        return builder.toString();
    }
}
