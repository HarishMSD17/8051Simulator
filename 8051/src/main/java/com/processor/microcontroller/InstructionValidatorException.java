package com.processor.microcontroller;

public class InstructionValidatorException extends RuntimeException{

    public InstructionValidatorException() {
        super();
    }

    public InstructionValidatorException(String s) {
        super(s);
    }

    public InstructionValidatorException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public InstructionValidatorException(Throwable throwable) {
            super(throwable);
        }
}
