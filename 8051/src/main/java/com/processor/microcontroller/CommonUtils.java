package com.processor.microcontroller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.RAM.CommonUtilsRAM;


public class CommonUtils {
    private static CommonUtils INSTANCE = new CommonUtils();
    private static final Logger LOGGER = Logger.getLogger(CommonUtils.class.getName());

    public static CommonUtils getInstance() {
        return INSTANCE;
    }

    private CommonUtils() {
    }

    public boolean isOperandImmediate(String operand) {
        return operand.startsWith("#");
    }

    public boolean isOperandRegister(String operand) {
        return operand.matches("^R[0-7]$");
    }

    public boolean isOperandDirect(String operand) {
        return operand.matches("^[0-9A-F]+[BHD]?$");
    }

    public boolean isOperandDirectComplement(String operand) {
        return operand.startsWith("/") && isOperandDirect(operand.replace("/", ""));
    }

    public boolean isOperandComplementDirect(String operand) {
        return operand.startsWith("/");
    }

    public boolean isOperandRegisterIndirect(String operand) { return operand.matches("^@R[0-2]$"); }

    public int getDecimalEquivalent(String str) {
        LOGGER.log(Level.FINER, "-->> getDecimalEquivalent() -> str: {0}", str);
        int decimalVal;

        if (str.endsWith("H")) {
            decimalVal = Integer.parseInt(str.replaceAll("[/H#]", ""), 16);
        } else if (str.endsWith("B")) {
            decimalVal = Integer.parseInt(str.replaceAll("[/B#]", ""), 2);
        } else {
            decimalVal = Integer.parseInt(str.replaceAll("[/D#]", ""));
        }

        LOGGER.log(Level.FINER, "<<-- getDecimalEquivalent()");

        return decimalVal;
    }

    public String getHexadecimalEquivalent(int decimalVal, int hexBitsPerRegister) {
        LOGGER.log(Level.FINER, "-->> getHexadecimalEquivalent() -> decimalVal: " + decimalVal + ", " +
                "hexBitsPerRegister: " + hexBitsPerRegister);

        StringBuilder hexResult = new StringBuilder(Integer.toHexString(decimalVal));
        while (hexResult.length() < hexBitsPerRegister) {
            hexResult.insert(0, "0");
        }
        hexResult.append("H");
        int length = hexResult.length();

        LOGGER.log(Level.FINER, "<<-- getHexadecimalEquivalent()");

        return hexResult.toString().toUpperCase().substring(length - hexBitsPerRegister - 1);
    }

    public String getHexadecimalEquivalent(int decimalVal) {

        return getHexadecimalEquivalent(decimalVal, Constants.HEXBITS_PER_REGISTER);
    }

    public String getHexadecimalEquivalent(String str) {
        return getHexadecimalEquivalent(getDecimalEquivalent(str), Constants.HEXBITS_PER_REGISTER);
    }

    public String getHexadecimalEquivalent(String str, int hexBitsPerRegister) {
        return getHexadecimalEquivalent(getDecimalEquivalent(str), hexBitsPerRegister);
    }

    public Map<String, String> getDataRange(Map<String, String> data, String strtAddr, String endAddr) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Map<String, String> subMap = new LinkedHashMap<String, String>();
        while (!strtAddr.equals(endAddr)) {
            subMap.put(strtAddr, data.get(strtAddr));
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 1);
        }
        subMap.put(strtAddr, data.get(strtAddr));
        return subMap;
    }

}
