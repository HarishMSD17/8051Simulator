package com.processor.microcontroller.RAM;

import java.util.Map;

import com.processor.microcontroller.Constants;

public class CommonUtilsRAM {

    private static final CommonUtilsRAM INSTANCE = new CommonUtilsRAM();

    private CommonUtilsRAM() {
    }

    public static CommonUtilsRAM getInstance() {
        return INSTANCE;
    }

    public void initializeData(Map<String, String> data, String startAddr, String endAddr) {

        int decimalStartAddr = Integer.parseInt(startAddr, 16);
        int decimalEndAddr = Integer.parseInt(endAddr, 16);

        for (;decimalStartAddr <= decimalEndAddr; decimalStartAddr++) {
            StringBuilder hexAddr = new StringBuilder();
            hexAddr.append(Integer.toHexString(decimalStartAddr).toUpperCase());
            if (hexAddr.length() % 2 != 0) {
                hexAddr.insert(0, "0");
            }
            data.put(hexAddr.toString(), "00H");
        }

    }

    public String getNewAddress(String strtAddr, int offset) {
        StringBuilder sb = new StringBuilder();
        int decimalAddress = Integer.parseInt(strtAddr, 16) + offset;
        sb.append(Integer.toHexString(decimalAddress));
        while (sb.length() < Constants.HEXBITS_PER_REGISTER) {
            sb.insert(0, '0');
        }
        return sb.toString().toUpperCase();
    }

    public boolean isBetween(String addr, String strtAddr, String endAddr) {
        int decimalStrtAddr = Integer.parseInt(strtAddr, 16);
        int decimalEndAddr = Integer.parseInt(endAddr, 16);
        int decimalAddr = Integer.parseInt(addr, 16);

        return decimalStrtAddr <= decimalAddr && decimalAddr <= decimalEndAddr;
    }
}
