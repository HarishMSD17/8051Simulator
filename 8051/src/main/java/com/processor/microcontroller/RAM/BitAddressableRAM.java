package com.processor.microcontroller.RAM;

import java.util.LinkedHashMap;
import java.util.Map;

import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.InstructionValidatorException;

public class BitAddressableRAM {

    private static final BitAddressableRAM INSTANCE = new BitAddressableRAM();
    private Map<String, String> data;

    private BitAddressableRAM() {
        data = new LinkedHashMap<String, String>();
        CommonUtilsRAM.getInstance().initializeData(data, Constants.BITADRESSABLE_START_ADDRESS, Constants
                .BITADRESSABLE_END_ADDRESS);
    }

    public static BitAddressableRAM getInstance() {
        return INSTANCE;
    }

    public String getData(String address) {
        return data.get(address);
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(String address, String value) {
        data.put(address, value);
    }

    public int getBitData(String bitAddr) {
        int decimalBitAddr = CommonUtils.getInstance().getDecimalEquivalent(bitAddr.replaceAll("/", ""));
        String byteAddr = CommonUtilsRAM.getInstance().getNewAddress(Constants.BITADRESSABLE_START_ADDRESS,
                decimalBitAddr / Constants.BITS_PER_REGISTER);
        int bitOffset = decimalBitAddr % Constants.BITS_PER_REGISTER;

        if (!CommonUtilsRAM.getInstance().isBetween(byteAddr, Constants.BITADRESSABLE_START_ADDRESS, Constants
                .BITADRESSABLE_END_ADDRESS)) {
            throw new InstructionValidatorException("Invalid BitAddress: " + bitAddr + "(byte:" + byteAddr + ") found");
        }
        int byteValue = CommonUtils.getInstance().getDecimalEquivalent(data.get(byteAddr));

        return (byteValue >> bitOffset) & 0b1;
    }

    public boolean getBitDataBool(String bitAddr) {
        return getBitData(bitAddr) == 1;
    }

    public void setBitDataBool(String bitAddr, boolean bitDataBool) {
        int bitData = bitDataBool ? 1 : 0;
        setBitData(bitAddr, bitData);
    }

    public void setBitData(String bitAddr, int bitData) {
        int decimalBitAddr = CommonUtils.getInstance().getDecimalEquivalent(bitAddr);
        String byteAddr = CommonUtilsRAM.getInstance().getNewAddress(Constants.BITADRESSABLE_START_ADDRESS,
                decimalBitAddr / Constants.BITS_PER_REGISTER);
        int bitOffset = decimalBitAddr % Constants.BITS_PER_REGISTER;

        if (!CommonUtilsRAM.getInstance().isBetween(byteAddr, Constants.BITADRESSABLE_START_ADDRESS, Constants
                .BITADRESSABLE_END_ADDRESS)) {
            throw new InstructionValidatorException("Invalid BitAddress: " + bitAddr + "(byte:" + byteAddr + ") found");
        }
        int byteValue = CommonUtils.getInstance().getDecimalEquivalent(data.get(byteAddr));
        if (bitData == 1) {
            byteValue |= 1 << bitOffset;
        } else {
            byteValue &= ~(1 << bitOffset);
        }
        data.put(byteAddr, CommonUtils.getInstance().getHexadecimalEquivalent(byteValue));
    }

    public void toggleBitData(String bitAddr) {
        int decimalBitAddr = CommonUtils.getInstance().getDecimalEquivalent(bitAddr);
        String byteAddr = CommonUtilsRAM.getInstance().getNewAddress(Constants.BITADRESSABLE_START_ADDRESS,
                decimalBitAddr / Constants.BITS_PER_REGISTER);

        if (CommonUtilsRAM.getInstance().isBetween(byteAddr, Constants.BITADRESSABLE_START_ADDRESS, Constants
                .BITADRESSABLE_END_ADDRESS)) {

            int bitOffset = decimalBitAddr % Constants.BITS_PER_REGISTER;
            int byteValue = CommonUtils.getInstance().getDecimalEquivalent(data.get(byteAddr));
            byteValue = byteValue ^ (1 << bitOffset);
            data.put(byteAddr, CommonUtils.getInstance().getHexadecimalEquivalent(byteValue));
        } else {
            throw new InstructionValidatorException("Invalid address: " + bitAddr + " found");
        }

    }

    /*
  * (non-Javadoc)
  * @see java.lang.Object#toString()
  */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
