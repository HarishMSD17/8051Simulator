package com.processor.microcontroller.RAM;

import java.util.LinkedHashMap;
import java.util.Map;

import com.processor.microcontroller.Constants;

public class ScratchPad {
    private static final ScratchPad INSTANCE = new ScratchPad();
    private Map<String, String> data;

    private ScratchPad() {
        data = new LinkedHashMap<String, String>();
        CommonUtilsRAM.getInstance().initializeData(data, Constants.SCRATCHPAD_START_ADDRESS, Constants
                .SCRATCHPAD_END_ADDRESS);
    }

    public static ScratchPad getInstance() {
        return INSTANCE;
    }

    public String getData(String address) {
        return data.get(address);
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(String address, String value) {
        data.put(address, value);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
