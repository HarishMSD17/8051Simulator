package com.processor.microcontroller.RAM;

import java.util.LinkedHashMap;
import java.util.Map;

public class RegisterBank {

    private Map<String, String> registers;
    private String strtAddr;
    private String endAddr;

    public RegisterBank() {
        registers = new LinkedHashMap<String, String>();
    }

    /**
     * @return the registers
     */
    public Map<String, String> getRegisters() {
        return registers;
    }

    /**
     * @param registers
     *         the registers to set
     */
    public void setRegisters(Map<String, String> registers) {
        this.registers = registers;
    }

    /**
     * @return the strtAddr
     */
    public String getStrtAddr() {
        return strtAddr;
    }

    /**
     * @param strtAddr
     *         the strtAddr to set
     */
    public void setStrtAddr(String strtAddr) {
        this.strtAddr = strtAddr;
    }

    /**
     * @return the endAddr
     */
    public String getEndAddr() {
        return endAddr;
    }

    /**
     * @param endAddr
     *         the endAddr to set
     */
    public void setEndAddr(String endAddr) {
        this.endAddr = endAddr;
    }

    public String getRegisterValue(int registerNum) {
        return registers.get(CommonUtilsRAM.getInstance().getNewAddress(strtAddr, registerNum));
    }

    public void setRegisterValue(int registerNum, String value) {
        registers.put(CommonUtilsRAM.getInstance().getNewAddress(strtAddr, registerNum), value);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tregisters: ").append(registers)
                .append("\n\tstrtAddr: ").append(strtAddr)
                .append("\n\tendAddr: ").append(endAddr)
                .append("\n}");
        return builder.toString();
    }
}
