package com.processor.microcontroller.RAM;

import java.util.Arrays;

import com.processor.microcontroller.Constants;
import com.processor.microcontroller.InstructionValidatorException;

public class RAM {

    private static final RAM INSTANCE = new RAM();

    private RegisterBank[] RB;
    private BitAddressableRAM bitAddrRAM;
    private ScratchPad scratchPad;

    private RAM() {
        RB = new RegisterBank[Constants.TOTAL_REGISTER_BANKS];
        initializeRegisterBanks();
        bitAddrRAM = BitAddressableRAM.getInstance();
        scratchPad = ScratchPad.getInstance();
    }

    private void initializeRegisterBanks() {
        String strtAddr = "00";

        for (int RBindex = 0; RBindex < Constants.TOTAL_REGISTER_BANKS; RBindex++) {
            RB[RBindex] = new RegisterBank();
            RB[RBindex].setStrtAddr(strtAddr);
            RB[RBindex].setEndAddr(CommonUtilsRAM.getInstance().getNewAddress(strtAddr, Constants.REGISTERS_PER_BANK
                    - 1));
            CommonUtilsRAM.getInstance().initializeData(RB[RBindex].getRegisters(), RB[RBindex].getStrtAddr(),
                    RB[RBindex].getEndAddr());
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, Constants.REGISTERS_PER_BANK);
        }
    }

    public static RAM getInstance() {
        return INSTANCE;
    }

    /**
     * @return the RB
     */
    public RegisterBank[] getRB() {
        return RB;
    }

    /**
     * @param RB
     *         the RB to set
     */
    public void setRB(RegisterBank[] RB) {
        this.RB = RB;
    }

    /**
     * @return the bitAddrRAM
     */
    public BitAddressableRAM getBitAddrRAM() {
        return bitAddrRAM;
    }

    /**
     * @param bitAddrRAM
     *         the bitAddrRAM to set
     */
    public void setBitAddrRAM(BitAddressableRAM bitAddrRAM) {
        this.bitAddrRAM = bitAddrRAM;
    }

    /**
     * @return the scratchPad
     */
    public ScratchPad getScratchPad() {
        return scratchPad;
    }

    /**
     * @param scratchPad
     *         the scratchPad to set
     */
    public void setScratchPad(ScratchPad scratchPad) {
        this.scratchPad = scratchPad;
    }

    public String getValue(String addr) {
        String value = null;
        boolean isFound = false;
        String address = addr.replaceAll("H", "");
        StringBuilder padAddr = new StringBuilder(address);
        while (padAddr.length() < Constants.HEXBITS_PER_REGISTER) {
            padAddr.insert(0, "0");
        }
        address = padAddr.toString();
        for (int RBindex = 0; RBindex < Constants.TOTAL_REGISTER_BANKS; RBindex ++) {
            if (CommonUtilsRAM.getInstance().isBetween(address, RB[RBindex].getStrtAddr(), RB[RBindex].getEndAddr())) {
                value = RB[RBindex].getRegisters().get(address);
                isFound = true;
                break;
            }
        }

        if (!isFound) {
            if (CommonUtilsRAM.getInstance().isBetween(address, Constants.BITADRESSABLE_START_ADDRESS, Constants
                    .BITADRESSABLE_END_ADDRESS)) {
                value = bitAddrRAM.getData(address);
                isFound = true;
            } else if (CommonUtilsRAM.getInstance().isBetween(address, Constants.SCRATCHPAD_START_ADDRESS, Constants
                    .SCRATCHPAD_END_ADDRESS)) {
                value = scratchPad.getData(address);
                isFound = true;
            }
        }

        if (!isFound) {
            throw new InstructionValidatorException("Address: " + address + " not found in RAM");
        }

        return value;
    }

    public void setValue(String addr, String value) {
        boolean isFound = false;
        String address = addr.replaceAll("H", "");
        StringBuilder padAddr = new StringBuilder(address);
        while (padAddr.length() < Constants.HEXBITS_PER_REGISTER) {
            padAddr.insert(0, "0");
        }
        address = padAddr.toString();
        for (int RBindex = 0; RBindex < Constants.TOTAL_REGISTER_BANKS; RBindex ++) {
            if (CommonUtilsRAM.getInstance().isBetween(address, RB[RBindex].getStrtAddr(), RB[RBindex].getEndAddr())) {
                RB[RBindex].getRegisters().put(address, value);
                isFound = true;
                break;
            }
        }

        if (!isFound) {
            if (CommonUtilsRAM.getInstance().isBetween(address, Constants.BITADRESSABLE_START_ADDRESS, Constants.BITADRESSABLE_END_ADDRESS)) {
                bitAddrRAM.setData(address, value);
                isFound = true;
            } else if (CommonUtilsRAM.getInstance().isBetween(address, Constants.SCRATCHPAD_START_ADDRESS, Constants.SCRATCHPAD_END_ADDRESS)) {
                scratchPad.setData(address, value);
                isFound = true;
            }
        }

        if (!isFound) {
            throw new InstructionValidatorException("Address: " + address + " not found in RAM");
        }
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tRB: ").append(Arrays.toString(RB))
                .append("\n\tbitAddrRAM: ").append(bitAddrRAM)
                .append("\n\tscratchPad: ").append(scratchPad)
                .append("\n}");
        return builder.toString();
    }
}
