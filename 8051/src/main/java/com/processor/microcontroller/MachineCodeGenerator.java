package com.processor.microcontroller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MachineCodeGenerator {

    private static final Logger LOGGER = Logger.getLogger(MachineCodeGenerator.class.getName());
    private static final MachineCodeGenerator INSTANCE = new MachineCodeGenerator();
    private Map<Instruction, OpcodeInfo> opcodeInfoMap;

    private MachineCodeGenerator() {
        try {
            opcodeInfoMap = getOpcodeInfoMap();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        LOGGER.log(Level.FINER, "OpcodeInfoMap: {0}", opcodeInfoMap);
    }

    public static MachineCodeGenerator getInstance() {
        return INSTANCE;
    }

    public List<MachineCode> generateMachineCode(List<Instruction> instructions) {
        LOGGER.log(Level.FINER, "-->> generateMachineCode() -> instructions: {0}", instructions.toString());

        List<MachineCode> machineCodes = new ArrayList<MachineCode>();
        String address = instructions.get(0).getMnemonic() == Mnemonic.ORG ? CommonUtils.getInstance()
                .getHexadecimalEquivalent(instructions.get(0).getOperands()[0], Constants.HEXBITS_PER_PC) : Constants
                .START_ADDRESS;
        address = address.replace("H", "");

        for (int i = 1; i < instructions.size() && instructions.get(i).getMnemonic() != Mnemonic.END; i++) {
            LOGGER.log(Level.FINER, "Generating Machine Code for the instruction: {0}", instructions.get(i));

            MachineCode machineCode = new MachineCode();
            String[] operands = instructions.get(i).getOperands();
            String[] operandsFormat = new String[operands.length];

            for (int j = 0; j < operands.length; j++) {
                operandsFormat[j] = getOperandType(operands[j]);
            }
            Instruction instructionFormat = new Instruction(instructions.get(i).getMnemonic(), operandsFormat);
            OpcodeInfo opcodeInfo = opcodeInfoMap.get(instructionFormat);
            if (opcodeInfo == null) {
                throw new InstructionValidatorException("Unable to generate opcode for " + instructionFormat.toString
                        ());
            }
            machineCode.setAddress(address);
            machineCode.setMachineLanguage(getMachineLanguage(opcodeInfo, operands, operandsFormat));
            machineCode.setAssemblyInstruction(instructions.get(i));
            machineCodes.add(machineCode);

            address = getAddress(address, opcodeInfo.getBytes());
        }

        LOGGER.log(Level.FINER, " <<-- generateMachineCode()");

        return machineCodes;
    }

    private String getAddress(String address, int bytes) {
        LOGGER.log(Level.FINER, "-->> getAddress() -> address: " + address + ", bytes: " + bytes);
        StringBuilder sb = new StringBuilder();
        int decimalAddress = Integer.parseInt(address, 16) + bytes;
        sb.append(Integer.toHexString(decimalAddress));
        while (sb.length() < Constants.HEXBITS_PER_PC) {
            sb.insert(0, '0');
        }
        LOGGER.log(Level.FINER, "<<-- getAddress()");
        return sb.toString().toUpperCase();
    }

    private String getOperandType(String operand) {

        LOGGER.log(Level.FINER, "-->> getOperandType() -> operand: {0}", operand);

        String operandFormat;

        if (Operand.ACCUMULATOR.getValue().equals(operand) || Operand.B.getValue().equals(operand) || Operand.C
                .getValue().equals(operand) ||
                Operand.AB.getValue().equals(operand) || Operand._R0.getValue().equals(operand) || Operand
                ._R1.getValue().equals(operand) ||
                Operand.R0.getValue().equals(operand) || Operand.R1.getValue().equals(operand) || Operand.R2.getValue
                ().equals(operand) ||
                Operand.R3.getValue().equals(operand) || Operand.R4.getValue().equals(operand) || Operand.R5.getValue
                ().equals(operand) ||
                Operand.R6.getValue().equals(operand) || Operand.R7.getValue().equals(operand) || Operand.A_PC
                .getValue().equals(operand) ||
                Operand.A_DPTR.getValue().equals(operand) || Operand.DPTR.getValue().equals(operand) || Operand._DPTR
                .getValue().equals(operand)) {
            operandFormat = operand;
        } else if (CommonUtils.getInstance().isOperandImmediate(operand)) {
            operandFormat = Operand.IMMEDIATE.getValue();
        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            operandFormat = Operand.DIRECT.getValue();
        } else if (CommonUtils.getInstance().isOperandDirectComplement(operand)) {
            operandFormat = Operand._DIRECT.getValue();
        } else {
            throw new InstructionValidatorException("Operand either invalid (or) not supported");
        }

        LOGGER.log(Level.FINER, " <<-- getOperandType()");

        return operandFormat;
    }

    private Map<Instruction, OpcodeInfo> getOpcodeInfoMap() throws IOException {
        LOGGER.log(Level.FINER, "-->> getOpcodeInfoMap() -> instructions");

        Map<Instruction, OpcodeInfo> opcodeInfoMap = new HashMap<Instruction, OpcodeInfo>();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(Constants.INSTRUCTIONSET_FILEPATH));
            String strLine = "";

            while ((strLine = br.readLine()) != null) {
                String[] inputStr = strLine.split(" ", 3);
                if (inputStr.length >= 3) {
                    OpcodeInfo opcodeInfo = new OpcodeInfo(inputStr[0], Integer.parseInt(inputStr[1]));
                    Instruction instruction = InstructionReader.getInstance().getInstruction(inputStr[2]);
                    opcodeInfoMap.put(instruction, opcodeInfo);
                } else {
                    LOGGER.log(Level.SEVERE, "Error while generating OpcodeInfo Map");
                    throw new InstructionValidatorException("Invalid instruction found when reading the string: " +
                            strLine + " from Opcodes file");
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while reading file", e);
        } finally {
            if (br != null) {
                br.close();
            }
        }

        LOGGER.log(Level.FINER, " <<-- getOpcodeInfoMap()");

        return opcodeInfoMap;
    }

    public String getMachineLanguage(OpcodeInfo opcodeInfo, String[] operands, String[] operandsFormat) {

        LOGGER.log(Level.FINER, "-->> getMachineLanguage() -> \nOpcodeInfo: " + opcodeInfo.toString() + "\nOperands: " +
                Arrays.toString(operands) + "\nOperandsFormat: " + Arrays.toString(operandsFormat));

        StringBuilder machineLanguage = new StringBuilder(opcodeInfo.getOpcode());

        for (int i = 0; i < operands.length; i++) {
            // Array values mismatch if the operand is any one of the registers
            if (!operands[i].equals(operandsFormat[i])) {
                int decimalNumber = CommonUtils.getInstance().getDecimalEquivalent(operands[i]);

                if (decimalNumber > Constants.MAX_VALUE) {
                    throw new InstructionValidatorException("Operand value cannot be more than " + Constants
                            .MAX_VALUE + ". Found: " + decimalNumber + "(" + operands[i] + ")");
                }
                String hexString = CommonUtils.getInstance().getHexadecimalEquivalent(decimalNumber);
                machineLanguage.append(hexString.replace("H", ""));
            }
        }

        LOGGER.log(Level.FINER, " <<-- getMachineLanguage()");

        return machineLanguage.toString();
    }
}
