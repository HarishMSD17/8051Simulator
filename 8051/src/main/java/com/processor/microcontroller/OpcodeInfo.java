package com.processor.microcontroller;

public class OpcodeInfo {

    private String opcode;
    private int bytes;

    public OpcodeInfo() {

    }

    public OpcodeInfo(String opcode, int bytes) {
        this.opcode = opcode;
        this.bytes = bytes;
    }

    /**
     * @return the opcode
     */
    public String getOpcode() {
        return opcode;
    }

    /**
     * @param opcode
     *         the opcode to set
     */
    public void setOpcode(String opcode) {
        this.opcode = opcode;
    }

    /**
     * @return the bytes
     */
    public int getBytes() {
        return bytes;
    }

    /**
     * @param bytes
     *         the bytes to set
     */
    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OpcodeInfo that = (OpcodeInfo) o;

        if (bytes != that.bytes) return false;
        return opcode.equals(that.opcode);
    }

    @Override
    public int hashCode() {
        int result = opcode.hashCode();
        result = 31 * result + bytes;
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\topcode: ").append(opcode)
                .append("\n\tbytes: ").append(bytes)
                .append("\n}");
        return builder.toString();
    }
}
