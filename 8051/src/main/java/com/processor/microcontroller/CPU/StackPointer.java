package com.processor.microcontroller.CPU;

import java.util.HashMap;
import java.util.Map;

import com.processor.microcontroller.Constants;

public class StackPointer {
    private static final StackPointer INSTANCE = new StackPointer();
    private Map<String, String> data;

    private StackPointer() {
        data = new HashMap<String, String>();
        data.put(Constants.STACKPOINTER_ADDRESS, Constants.STACKPOINTER_INITIAL_VALUE);
    }

    public static StackPointer getInstance() {
        return INSTANCE;
    }

    public String getData() {
        return data.get(Constants.STACKPOINTER_ADDRESS);
    }

    public void setData(String data) {
        this.data.put(Constants.STACKPOINTER_ADDRESS, data);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
