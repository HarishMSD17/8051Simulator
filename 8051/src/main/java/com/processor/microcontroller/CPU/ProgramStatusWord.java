package com.processor.microcontroller.CPU;

import java.util.HashMap;
import java.util.Map;

import com.processor.microcontroller.Constants;

public class ProgramStatusWord {

    private static final ProgramStatusWord INSTANCE = new ProgramStatusWord();
    private Map<String, boolean[]> flags;

    private ProgramStatusWord() {
        flags = new HashMap<String, boolean[]>();
        flags.put(Constants.PSW_ADDRESS, new boolean[Constants.PSW_SIZE]);
    }

    public static ProgramStatusWord getInstance() {
        return INSTANCE;
    }

    public boolean getFlag(PSWFlag flag) {
        return flags.get(Constants.PSW_ADDRESS)[flag.getFlagPosition()];
    }

    public void setFlag(PSWFlag flag) {
        boolean[] reg = flags.get(Constants.PSW_ADDRESS);
        reg[flag.getFlagPosition()] = true;
        flags.put(Constants.PSW_ADDRESS, reg);
    }

    public void resetFlag(PSWFlag flag) {
        boolean[] reg = flags.get(Constants.PSW_ADDRESS);
        reg[flag.getFlagPosition()] = false;
        flags.put(Constants.PSW_ADDRESS, reg);
    }

    public void toggleFlag(PSWFlag flag) {
        boolean[] reg = flags.get(Constants.PSW_ADDRESS);
        reg[flag.getFlagPosition()] = !reg[flag.getFlagPosition()];
        flags.put(Constants.PSW_ADDRESS, reg);
    }

    private boolean isSet(PSWFlag flag) {
        return flags.get(Constants.PSW_ADDRESS)[flag.getFlagPosition()];
    }

    public int getFlagValue(PSWFlag flag) {
       return flags.get(Constants.PSW_ADDRESS)[flag.getFlagPosition()] ? 1 : 0;
    }

    public void setFlagValue(PSWFlag flag, boolean flagBool) {
        boolean[] reg = flags.get(Constants.PSW_ADDRESS);
        reg[flag.getFlagPosition()] = flagBool;
        flags.put(Constants.PSW_ADDRESS, reg);
    }

    public void setFlagValue(PSWFlag flag, int flagValue) {
        setFlagValue(flag, (flagValue == 1));

    }

    public int getCurrentBank() {
        boolean RS1 = isSet(PSWFlag.RS1);
        boolean RS0 = isSet((PSWFlag.RS0));
        int bankNum;

        if (!RS1 && !RS0) {
            bankNum = 0;
        } else if (!RS1 && RS0) {
            bankNum = 1;
        } else if (RS1 && !RS0) {
            bankNum = 2;
        } else {
            bankNum = 3;
        }

        return bankNum;
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tflags: ").append(flags)
                .append("\n}");
        return builder.toString();
    }
}
