package com.processor.microcontroller.CPU;

import com.processor.microcontroller.Constants;

public class ProgramCounter {
    private static final ProgramCounter INSTANCE = new ProgramCounter();
    private String PC = Constants.PC_START_VALUE;

    private ProgramCounter() {

    }

    public static ProgramCounter getInstance() {
        return INSTANCE;
    }

    /**
     * @return the PC
     */
    public String getValue() {
        return PC;
    }

    /**
     * @param PC
     *         the PC to set
     */
    public void setValue(String PC) {
        this.PC = PC;
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tPC: ").append(PC)
                .append("\n}");
        return builder.toString();
    }
}
