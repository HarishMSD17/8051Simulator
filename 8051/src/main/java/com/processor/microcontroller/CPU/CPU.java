package com.processor.microcontroller.CPU;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.CPU.reader.InstructionReader;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.MachineCode;
import com.processor.microcontroller.RAM.CommonUtilsRAM;
import com.processor.microcontroller.RAM.RAM;

public class CPU {

    private static final Logger LOGGER = Logger.getLogger(CPU.class.getName());
    private static final CPU INSTANCE = new CPU();

    private Accumulator ACC;
    private BRegister B;
    private DataPointer DPTR;
    private ProgramCounter PC;
    private ProgramStatusWord PSW;
    private StackPointer SP;

    private CPU() {
        ACC = Accumulator.getInstance();
        B = BRegister.getInstance();
        DPTR = DataPointer.getInstance();
        PC = ProgramCounter.getInstance();
        PSW = ProgramStatusWord.getInstance();
        SP = StackPointer.getInstance();
    }

    public static CPU getInstance() {
        return INSTANCE;
    }

    public void processInstructions(List<MachineCode> machineCodes, String destination) {
        LOGGER.log(Level.FINER, "-->> processInstructions()");
        try {
            for (MachineCode machineCode : machineCodes) {
                InstructionReader instructionReader = InstructionReader.getInstructionReader(machineCode.getAssemblyInstruction());
                instructionReader.executeInstruction(machineCode.getAssemblyInstruction());
                getPC().setValue(machineCode.getAddress());
                writeCurrentStatusToFile(destination, machineCode);
            }
        } catch (InstructionValidatorException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        System.out.println("Check Results.txt file for the output");
        LOGGER.log(Level.FINER, "<<-- processInstructions()");
    }

    private void writeCurrentStatusToFile(String destination, MachineCode machineCode) throws IOException {
        File file = new File(destination);
        BufferedWriter bufferedWriter = null;
        try {
            //Instruction
            bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write(" " +
                    "\n\n--------------------------------------------------------------------------------\n");
            bufferedWriter.write(String.format("%-75s", "|   Instruction   |   " + machineCode.getAssemblyInstruction
                    ().toString()) +
                    "     |\n");
            bufferedWriter.write(" -------------------------------------------------------------------------------\n" +
                    "\n\n");

            //CPU
            bufferedWriter.write(" -------------------------------------------------------------------------------\n");
            bufferedWriter.write("|   " + String.format("%-6s", "ACC") + "   |  " + String.format("%-64s", ACC
                    .getValue()) + "|\n");
            bufferedWriter.write("|-------------------------------------------------------------------------------|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "B") + "   |  " + String.format("%-64s", B
                    .getValue()) + "|\n");
            bufferedWriter.write("|-------------------------------------------------------------------------------|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "DPTR") + "   |  " + String.format("%-64s",
                    DPTR.getValue()) + "|\n");
            bufferedWriter.write("|-------------------------------------------------------------------------------|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "PC") + "   |  " + String.format("%-64s", PC
                    .getValue()) + "|\n");
            bufferedWriter.write("|-------------------------------------------------------------------------------|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "SP") + "   |  " + String.format("%-64s", SP
                    .getData()) + "|\n");
            bufferedWriter.write("|-------------------------------------------------------------------------------|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "") + "   |  " + String.format("%-64s", " " +
                    "------------------------------- ") + "|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "PSW") + "   |  ");
            bufferedWriter.write(String.format("%-64s", "| " + PSW.getFlagValue(PSWFlag.CY) + " | " + PSW
                    .getFlagValue(PSWFlag.AC) + " | " + PSW.getFlagValue(PSWFlag.F0) +
                    " | " + PSW.getFlagValue(PSWFlag.RS1) + " | " + PSW.getFlagValue(PSWFlag.RS0) + " | " + PSW
                    .getFlagValue(PSWFlag.OV) + " | " + PSW.getFlagValue(PSWFlag.UD) +
                    " | " + PSW.getFlagValue(PSWFlag.P) + " |") + "|\n");
            bufferedWriter.write("|   " + String.format("%-6s", "") + "   | " + String.format("%-64s", " " +
                    " ------------------------------- ") + " |\n");
            bufferedWriter.write(" -------------------------------------------------------------------------------\n" +
                    "\n\n");

            bufferedWriter.write(" -------------------------------------------------------------------------------\n");

            //RAM
            bufferedWriter.write("|   RB   |" + String.format("%70s", "") + "|\n");
            bufferedWriter.write(" ------- " + String.format("%71s", "") + "|\n");
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", RAM.getInstance().getRB()
                    [0].getRegisters()) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", RAM.getInstance().getRB()
                    [1].getRegisters()) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", RAM.getInstance().getRB()
                    [2].getRegisters()) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", RAM.getInstance().getRB()
                    [3].getRegisters()) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            bufferedWriter.write(" -------------------------------------------------------------------------------\n");

            bufferedWriter.write("|   BitAddr RAM   |" + String.format("%61s", "") + "|\n");
            bufferedWriter.write(" ------------------- " + String.format("%59s", "") + "|\n");
            String strtAddr = Constants.BITADRESSABLE_START_ADDRESS;
            String endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getBitAddrRAM().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getBitAddrRAM().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write(" -------------------------------------------------------------------------------\n");

            bufferedWriter.write("|   Scratch Pad   |" + String.format("%61s", "") + "|\n");
            bufferedWriter.write(" ------------------- " + String.format("%59s", "") + "|\n");
            strtAddr = Constants.SCRATCHPAD_START_ADDRESS;
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write("|" + String.format("%79s", "") + "|\n");
            strtAddr = CommonUtilsRAM.getInstance().getNewAddress(endAddr, 1);
            endAddr = CommonUtilsRAM.getInstance().getNewAddress(strtAddr, 7);
            bufferedWriter.write("|" + String.format("%10s", "") + String.format("%-69s", CommonUtils.getInstance()
                    .getDataRange(RAM.getInstance().getScratchPad().getData(), strtAddr, endAddr)) + "|\n");
            bufferedWriter.write(" -------------------------------------------------------------------------------\n" +
                    "\n");
            bufferedWriter.write(" " +
                    "==========================================================================================================================================================================");

        } catch (IOException e) {
            throw e;
        } finally {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
        }
    }

    /**
     * @return the ACC
     */
    public Accumulator getACC() {
        return ACC;
    }

    /**
     * @param ACC
     *         the ACC to set
     */
    public void setACC(Accumulator ACC) {
        this.ACC = ACC;
    }

    /**
     * @return the B
     */
    public BRegister getB() {
        return B;
    }

    /**
     * @param B
     *         the B to set
     */
    public void setB(BRegister B) {
        this.B = B;
    }

    /**
     * @return the DPTR
     */
    public DataPointer getDPTR() {
        return DPTR;
    }

    /**
     * @param DPTR
     *         the DPTR to set
     */
    public void setDPTR(DataPointer DPTR) {
        this.DPTR = DPTR;
    }

    /**
     * @return the PC
     */
    public ProgramCounter getPC() {
        return PC;
    }

    /**
     * @param PC
     *         the PC to set
     */
    public void setPC(ProgramCounter PC) {
        this.PC = PC;
    }

    /**
     * @return the PSW
     */
    public ProgramStatusWord getPSW() {
        return PSW;
    }

    /**
     * @param PSW
     *         the PSW to set
     */
    public void setPSW(ProgramStatusWord PSW) {
        this.PSW = PSW;
    }

    /**
     * @return the SP
     */
    public StackPointer getSP() {
        return SP;
    }

    /**
     * @param SP
     *         the SP to set
     */
    public void setSP(StackPointer SP) {
        this.SP = SP;
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n  ACC: ").append(ACC)
                .append("\n  B: ").append(B)
                .append("\n  DPTR: ").append(DPTR)
                .append("\n  PC: ").append(PC)
                .append("\n  PSW: ").append(PSW)
                .append("\n  SP: ").append(SP)
                .append("\n}");
        return builder.toString();
    }
}
