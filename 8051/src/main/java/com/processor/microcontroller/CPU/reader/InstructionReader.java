package com.processor.microcontroller.CPU.reader;

import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.Mnemonic;

public abstract class InstructionReader {

    public InstructionReader() {

    }

    public static InstructionReader getInstructionReader(Instruction instruction) {
        InstructionReader instructionReader;

        if (isArithmeticInstruction(instruction)) {
            instructionReader = ArithmeticInstructionReader.getInstance();
        } else if (isBranchInstruction(instruction)) {
            instructionReader = BranchInstructionReader.getInstance();
        } else if (isDataTransferInstruction(instruction)) {
            instructionReader = DataTransferInstruction.getInstance();
        } else if (isLogicalInstruction(instruction)) {
            instructionReader = LogicalInstructionReader.getInstance();
        } else if (isBooleanInstruction(instruction)) {
            instructionReader = BooleanInstructionReader.getInstance();
        } else {
            throw new InstructionValidatorException("Invalid instruction: " + instruction + " found");
        }

        return instructionReader;
    }

    private static boolean isArithmeticInstruction(Instruction instruction) {
        boolean success = false;

        if (instruction.getMnemonic() == Mnemonic.ADD || instruction.getMnemonic() == Mnemonic.ADDC || instruction.getMnemonic() == Mnemonic.SUBB ||
                instruction.getMnemonic() == Mnemonic.INC || instruction.getMnemonic() == Mnemonic.DEC || instruction.getMnemonic() == Mnemonic.MUL ||
                instruction.getMnemonic() == Mnemonic.DIV || instruction.getMnemonic() == Mnemonic.DA) {
            success = true;
        }

        return success;
    }

    private static boolean isBranchInstruction(Instruction instruction) {
        boolean success = false;

        if (instruction.getMnemonic() == Mnemonic.ACALL || instruction.getMnemonic() == Mnemonic.LCALL || instruction.getMnemonic() == Mnemonic.RET ||
                instruction.getMnemonic() == Mnemonic.AJMP || instruction.getMnemonic() == Mnemonic.LJMP || instruction.getMnemonic() == Mnemonic.SJMP ||
                instruction.getMnemonic() == Mnemonic.JC || instruction.getMnemonic() == Mnemonic.JNC || instruction.getMnemonic() == Mnemonic.JB ||
                instruction.getMnemonic() == Mnemonic.JBC || instruction.getMnemonic() == Mnemonic.JMP || instruction.getMnemonic() == Mnemonic.JZ ||
                instruction.getMnemonic() == Mnemonic.JNZ || instruction.getMnemonic() == Mnemonic.CJNE || instruction.getMnemonic() == Mnemonic.DJNZ ||
                instruction.getMnemonic() == Mnemonic.NOP) {
            success = true;
        }

        return success;
    }

    private static boolean isDataTransferInstruction(Instruction instruction) {
        boolean success = false;

        if ((instruction.getMnemonic() == Mnemonic.MOV && !(instruction.getOperands()[0].contains(Constants.CARRY_FLAG) || instruction.getOperands()[1].contains(Constants.CARRY_FLAG))) ||
                instruction.getMnemonic() == Mnemonic.MOVC || instruction.getMnemonic() == Mnemonic.MOVX || instruction.getMnemonic() == Mnemonic.PUSH ||
                instruction.getMnemonic() == Mnemonic.POP || instruction.getMnemonic() == Mnemonic.XCH || instruction.getMnemonic() == Mnemonic.XCHD) {
            success = true;
        }

        return success;
    }

    private static boolean isLogicalInstruction(Instruction instruction) {
        boolean success = false;

        if ((instruction.getMnemonic() == Mnemonic.ANL && !(instruction.getOperands()[0].contains(Constants.CARRY_FLAG) || instruction.getOperands()[1].contains(Constants.CARRY_FLAG)))
                || (instruction.getMnemonic() == Mnemonic.ORL && !(instruction.getOperands()[0].contains(Constants.CARRY_FLAG) || instruction.getOperands()[1].contains(Constants.CARRY_FLAG))) ||
                instruction.getMnemonic() == Mnemonic.XRL || (instruction.getMnemonic() == Mnemonic.CLR && instruction.getOperands()[0].contains(Constants.ACCUMULATOR))
                || (instruction.getMnemonic() == Mnemonic.CPL && instruction.getOperands()[0].contains(Constants.ACCUMULATOR)) || instruction.getMnemonic() == Mnemonic.SWAP ||
                instruction.getMnemonic() == Mnemonic.RL || instruction.getMnemonic() == Mnemonic.RLC || instruction.getMnemonic() == Mnemonic.RR || instruction.getMnemonic() == Mnemonic.RRC) {
            success = true;
        }

        return success;
    }

    private static boolean isBooleanInstruction(Instruction instruction) {
        boolean success = false;

        if ((instruction.getMnemonic() == Mnemonic.CLR && (instruction.getOperands()[0].contains(Constants
                .CARRY_FLAG) || CommonUtils.getInstance().isOperandDirect(instruction.getOperands()[0]))) ||
                instruction.getMnemonic() == Mnemonic.SETB || (instruction.getMnemonic() == Mnemonic.CPL &&
                (instruction.getOperands()[0].contains(Constants.CARRY_FLAG) || CommonUtils.getInstance()
                        .isOperandDirect(instruction.getOperands()[0]))) ||
                (instruction.getMnemonic() == Mnemonic.ANL && instruction.getOperands()[0].contains(Constants.CARRY_FLAG)) || (instruction.getMnemonic() == Mnemonic.ORL && instruction.getOperands()[0].contains(Constants.CARRY_FLAG)) ||
                (instruction.getMnemonic() == Mnemonic.MOV && (instruction.getOperands()[0].contains(Constants
                        .CARRY_FLAG) || CommonUtils.getInstance().isOperandDirect(instruction.getOperands()[0])))) {
                    success = true;
        }

        return success;
    }

    public abstract void executeInstruction(Instruction instruction);
}
