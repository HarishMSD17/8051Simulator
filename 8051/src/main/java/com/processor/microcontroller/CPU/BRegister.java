package com.processor.microcontroller.CPU;

import java.util.HashMap;
import java.util.Map;

import com.processor.microcontroller.Constants;

public class BRegister {
    private static final BRegister INSTANCE = new BRegister();
    private Map<String, String> data;

    private BRegister() {
        data = new HashMap<String, String>();
        data.put(Constants.BREG_ADDRESS, Constants.BREG_START_VALUE);
    }

    public static BRegister getInstance() {
        return INSTANCE;
    }

    public String getValue() {
        return data.get(Constants.BREG_ADDRESS);
    }

    public void setValue(String data) {
        this.data.put(Constants.BREG_ADDRESS, data);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
