package com.processor.microcontroller.CPU;

import java.util.HashMap;
import java.util.Map;

import com.processor.microcontroller.Constants;

public class Accumulator {

    private static final Accumulator INSTANCE = new Accumulator();
    private Map<String, String> data;

    private Accumulator() {
        data = new HashMap<String, String>();
        data.put(Constants.ACCUMULATOR_ADDRESS, Constants.ACCUMULATOR_INITIAL_VALUE);
    }

    public static Accumulator getInstance() {
        return INSTANCE;
    }

    public String getValue() {
        return data.get(Constants.ACCUMULATOR_ADDRESS);
    }

    public void setData(String data) {
        this.data.put(Constants.ACCUMULATOR_ADDRESS, data);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
