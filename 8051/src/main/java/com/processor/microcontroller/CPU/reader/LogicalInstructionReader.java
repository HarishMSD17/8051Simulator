package com.processor.microcontroller.CPU.reader;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.CPU.CPU;
import com.processor.microcontroller.CPU.PSWFlag;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.Operand;
import com.processor.microcontroller.RAM.RAM;

public class LogicalInstructionReader extends InstructionReader {

    private static final LogicalInstructionReader INSTANCE = new LogicalInstructionReader();
    private static final Logger LOGGER = Logger.getLogger(LogicalInstructionReader.class.getName());
    private CPU cpu;
    private RAM ram;

    public static LogicalInstructionReader getInstance() {
        return INSTANCE;
    }

    private LogicalInstructionReader() {
        cpu = CPU.getInstance(); ram = RAM.getInstance();
    }

    public void executeInstruction(Instruction instruction) {
        LOGGER.log(Level.FINER, "-->> executeInstructions() -> instruction: {0}", instruction);

        switch (instruction.getMnemonic()) {
            case ANL:
                ANL(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case ORL:
                ORL(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case XRL:
                XRL(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case CLR:
                CLR(instruction.getOperands()[0]);
                break;

            case CPL:
                CPL(instruction.getOperands()[0]);
                break;

            case SWAP:
                SWAP(instruction.getOperands()[0]);
                break;

            case RL:
                RL(instruction.getOperands()[0]);
                break;

            case RLC:
                RLC(instruction.getOperands()[0]);
                break;

            case RR:
                RR(instruction.getOperands()[0]);
                break;

            case RRC:
                RRC(instruction.getOperands()[0]);
                break;

            default:
                throw new InstructionValidatorException("Invalid Mnemonic: " + instruction.getMnemonic().name() + " found for the Logical instruction: " + instruction.toString());
        }

        LOGGER.log(Level.FINER, "<<-- executeInstructions()");
    }
    
    private void ANL(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ANL() -> operand1: " + operand1 + ", operand2: " + operand2);
        String result;

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            int acc = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());

            if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                int registerValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance()
                        .getRegisterValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc & registerValue);

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc & directValue);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc & immediateValue);

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                int addrValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance()
                        .getRegisterIndirectValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc & addrValue);

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            cpu.getACC().setData(result);
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else if (CommonUtils.getInstance().isOperandDirect(operand1)) {
            int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand1));

            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                int acc = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue & acc);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue & immediateValue);

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            ram.setValue(operand1, result);
        }

        LOGGER.log(Level.FINER, "<<-- ANL()");
    }
    
    private void ORL(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ORL() -> operand1: " + operand1 + ", operand2: " + operand2);
        String result;

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            int acc = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());

            if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                int registerValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc | registerValue);

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc | directValue);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc | immediateValue);

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                int addrValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc | addrValue);
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            cpu.getACC().setData(result);
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else if (CommonUtils.getInstance().isOperandDirect(operand1)) {
            int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand1));

            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                int acc = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue | acc);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue | immediateValue);

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            ram.setValue(operand1, result);
        }
        LOGGER.log(Level.FINER, "<<-- ORL()");

    }
    
    private void XRL(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> XRL() -> operand1: " + operand1 + ", operand2: " + operand2);
        String result;

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            int acc = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());

            if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                int registerValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc ^ registerValue);

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc ^ directValue);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc ^ immediateValue);

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                int addrValue = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(acc ^ addrValue);
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
            cpu.getACC().setData(result);
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else if (CommonUtils.getInstance().isOperandDirect(operand1)) {
            int directValue = CommonUtils.getInstance().getDecimalEquivalent(ram.getValue(operand1));

            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                int acc = CommonUtils.getInstance().getDecimalEquivalent(operand1);
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue ^ acc);

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                int immediateValue = CommonUtils.getInstance().getDecimalEquivalent(operand2.replaceAll("#", ""));
                result = CommonUtils.getInstance().getHexadecimalEquivalent(directValue ^ immediateValue);

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            ram.setValue(operand1, result);
        }
        LOGGER.log(Level.FINER, "<<-- XRL()");

    }

    private void CLR(String operand) {
        LOGGER.log(Level.FINER, "-->> CLR() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            cpu.getACC().setData("00H");
            cpu.getPSW().resetFlag(PSWFlag.P);

        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- CLR()");

    }

    private void CPL(String operand) {
        LOGGER.log(Level.FINER, "-->> CPL() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal ^ Constants.MAX_VALUE));
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- CPL()");
    }

    private void SWAP(String operand) {
        LOGGER.log(Level.FINER, "-->> SWAP() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            String acc = cpu.getACC().getValue();
            acc = acc.substring(Constants.ACCUMULATOR_SIZE_HEX / 2, acc.length() - 1) + acc.substring(0, Constants
                    .ACCUMULATOR_SIZE_HEX / 2) + "H";
            cpu.getACC().setData(acc);
        }

        LOGGER.log(Level.FINER, "<<-- SWAP()");

    }

    private void RL(String operand) {
        LOGGER.log(Level.FINER, "-->> RL() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            decimalVal = decimalVal << 1 | decimalVal >> Constants.BITS_PER_REGISTER - 1;
            decimalVal = decimalVal & Constants.MAX_VALUE_HEX;      //eliminate the higher order bits
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
        }

        LOGGER.log(Level.FINER, "<<-- RL()");
    }

    private void RLC(String operand) {
        LOGGER.log(Level.FINER, "-->> RLC() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            int newCY = (decimalVal >> Constants.BITS_PER_REGISTER - 1) & 0b1;
            decimalVal = decimalVal << 1 | cpu.getPSW().getFlagValue(PSWFlag.CY);
            decimalVal = decimalVal & Constants.MAX_VALUE_HEX;
            if (newCY == 1) {
                cpu.getPSW().setFlag(PSWFlag.CY);
            } else {
                cpu.getPSW().resetFlag(PSWFlag.CY);
            }
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());
        }

        LOGGER.log(Level.FINER, "<<-- RLC()");
    }

    private void RR(String operand) {
        LOGGER.log(Level.FINER, "-->> RR() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            decimalVal = decimalVal >> 1 | decimalVal << Constants.BITS_PER_REGISTER - 1;
            decimalVal = decimalVal & Constants.MAX_VALUE_HEX;      //eliminate the higher order bits
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
        }

        LOGGER.log(Level.FINER, "<<-- RR()");
    }

    private void RRC(String operand) {
        LOGGER.log(Level.FINER, "-->> RRC() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            int newCY = decimalVal & 0b1;
            decimalVal = decimalVal >> 1 | (cpu.getPSW().getFlagValue(PSWFlag.CY) << Constants.BITS_PER_REGISTER - 1);
            decimalVal = decimalVal & Constants.MAX_VALUE_HEX;
            if (newCY == 1) {
                cpu.getPSW().setFlag(PSWFlag.CY);
            } else {
                cpu.getPSW().resetFlag(PSWFlag.CY);
            }
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());
        }

        LOGGER.log(Level.FINER, "<<-- RRC()");

    }
}
