package com.processor.microcontroller.CPU.reader;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.CPU.CPU;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.Operand;
import com.processor.microcontroller.RAM.RAM;

public class DataTransferInstruction extends InstructionReader {

    private static final DataTransferInstruction INSTANCE = new DataTransferInstruction();
    private static final Logger LOGGER = Logger.getLogger(BranchInstructionReader.class.getName());
    private CPU cpu;
    private RAM ram;

    public static DataTransferInstruction getInstance() {
        return INSTANCE;
    }

    private DataTransferInstruction() {
        cpu = CPU.getInstance();
        ram = RAM.getInstance();
    }

    public void executeInstruction(Instruction instruction) {
        LOGGER.log(Level.FINER, "-->> executeInstructions() -> instruction: " + instruction);

        switch (instruction.getMnemonic()) {
            case MOV:
                MOV(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case MOVC:
                MOVC(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case MOVX:
                MOVX(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case PUSH:
                PUSH(instruction.getOperands()[0]);
                break;

            case POP:
                POP(instruction.getOperands()[1]);
                break;

            case XCH:
                XCH(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case XCHD:
                XCHD(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            default:
                throw new InstructionValidatorException("Invalid Mnemonic: " + instruction.getMnemonic().name() + " " +
                        "found for the DataTransfer instruction: " + instruction.toString());
        }

        LOGGER.log(Level.FINER, "<<-- executeInstructions()");
    }

    public void MOV(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> MOV() -->> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                cpu.getACC().setData(CommonUtilsReader.getInstance().getRegisterValue(operand2));

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                cpu.getACC().setData(ram.getValue(CommonUtils.getInstance().getHexadecimalEquivalent(operand2)));

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                cpu.getACC().setData(CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(CommonUtils.getInstance()
                        .getDecimalEquivalent(operand2)));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else if (CommonUtils.getInstance().isOperandRegister(operand1)) {

            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                CommonUtilsReader.getInstance().setRegisterValue(operand1, cpu.getACC().getValue());
            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                CommonUtilsReader.getInstance().setRegisterValue(operand1, ram.getValue(CommonUtils.getInstance()
                        .getHexadecimalEquivalent
                        (operand2)));

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                CommonUtilsReader.getInstance().setRegisterValue(operand1, CommonUtils.getInstance().getHexadecimalEquivalent(CommonUtils
                        .getInstance().getDecimalEquivalent(operand2)));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else if (CommonUtils.getInstance().isOperandDirect(operand1)) {
            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                ram.setValue(operand1, cpu.getACC().getValue());

            } else if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                ram.setValue(operand1, CommonUtilsReader.getInstance().getRegisterValue(operand2));

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                ram.setValue(operand1, ram.getValue(CommonUtils.getInstance().getHexadecimalEquivalent(operand2)));

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                ram.setValue(operand1, CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));

            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                ram.setValue(operand1, CommonUtils.getInstance().getHexadecimalEquivalent(operand2));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand1)) {
            int registerNum = Integer.parseInt(operand1.replace("@R", ""));
            int currentBank = cpu.getPSW().getCurrentBank();
            String registerValue = ram.getRB()[currentBank].getRegisterValue(registerNum);

            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                ram.setValue(registerValue, cpu.getACC().getValue());

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                ram.setValue(registerValue, ram.getValue(CommonUtils.getInstance().getHexadecimalEquivalent(operand2)));
            } else if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                ram.setValue(registerValue, CommonUtils.getInstance().getHexadecimalEquivalent(CommonUtils
                        .getInstance().getDecimalEquivalent(operand2)));
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }

        } else if (Operand.DPTR.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                cpu.getDPTR().setValue(operand2.replace("#", ""));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }

        LOGGER.log(Level.FINER, "<<-- MOV()");

    }

    public void MOVC(String operand1, String operand2) {
        throw new InstructionValidatorException("MOVC instruction is currently not supported by MC8051");

    }

    public void MOVX(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> MOVX() -->> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            throw new InstructionValidatorException("This instruction is currently not supported by MC8051");

        } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand1)) {
            if (Operand.ACCUMULATOR.getValue().equals(operand2)) {
                CommonUtilsReader.getInstance().setRegisterIndirectValue(operand1, cpu.getACC().getValue());
            }
        } else if (Operand._DPTR.getValue().equals(operand1)) {
            throw new InstructionValidatorException("This instruction is currently not supported by MC8051");

        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }
        LOGGER.log(Level.FINER, "<<-- MOVX()");

    }

    public void PUSH(String operand) {
        throw new InstructionValidatorException("PUSH instruction is currently not supported by MC8051");

    }

    public void POP(String operand) {
        throw new InstructionValidatorException("POP instruction is currently not supported by MC8051");

    }

    public void XCH(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> XCH() -->> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            String temp = cpu.getACC().getValue();

            if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                int registerNum = Integer.parseInt(operand2.replace("R", ""));
                int currentBank = cpu.getPSW().getCurrentBank();
                cpu.getACC().setData(ram.getRB()[currentBank].getRegisterValue(registerNum));
                ram.getRB()[currentBank].setRegisterValue(registerNum, temp);

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                String directAddr = CommonUtils.getInstance().getHexadecimalEquivalent(operand2);
                cpu.getACC().setData(ram.getValue(directAddr));
                ram.setValue(directAddr, temp);

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                int registerNum = Integer.parseInt(operand2.replace("@R", ""));
                int currentBank = cpu.getPSW().getCurrentBank();
                String registerValue = ram.getRB()[currentBank].getRegisterValue(registerNum);
                cpu.getACC().setData(ram.getValue(registerValue));
                ram.setValue(registerValue, temp);
            } else {
                throw new InstructionValidatorException("Invalid Opearnd: " + operand2 + " found");
            }

            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }

        CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        LOGGER.log(Level.FINER, "<<-- XCH()");

    }

    public void XCHD(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> XCHD() -->> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            String acc = cpu.getACC().getValue();
            int accDecimal = CommonUtils.getInstance().getDecimalEquivalent(acc);
            int tempDecimal = accDecimal;
            if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                int valueDecimal = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance()
                        .getRegisterIndirectValue(operand2));
                accDecimal &= 0b11110000;
                accDecimal |= (valueDecimal & 0b1111);
                valueDecimal &= 0b11110000;
                valueDecimal |= (tempDecimal & 0b1111);
                CommonUtilsReader.getInstance().setRegisterIndirectValue(operand2, CommonUtils.getInstance()
                        .getHexadecimalEquivalent(valueDecimal));
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }

            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(accDecimal));
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }


        LOGGER.log(Level.FINER, "<<-- XCHD()");

    }
    
    
}
