package com.processor.microcontroller.CPU.reader;

import com.processor.microcontroller.CPU.CPU;
import com.processor.microcontroller.CPU.PSWFlag;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.RAM.RAM;

public class CommonUtilsReader {
    private static final CommonUtilsReader INSTANCE = new CommonUtilsReader();
    CPU cpu;

    public static CommonUtilsReader getInstance() {
        return INSTANCE;
    }

    private CommonUtilsReader() {
        cpu = CPU.getInstance();
    }

    public void setResetCY(int value) {

        if (value > Constants.MAX_VALUE || value < 0) {
            cpu.getPSW().setFlag(PSWFlag.CY);
        } else {
            cpu.getPSW().resetFlag(PSWFlag.CY);
        }
    }

    public void setResetParity(int value) {

        String binaryString = Integer.toBinaryString(value);
        if (binaryString.replaceAll("0", "").length() % 2 != 0) {
             cpu.getPSW().setFlag(PSWFlag.P);
         } else
         {
             cpu.getPSW().resetFlag(PSWFlag.P);
         }
    }

    public void setResetParity(String value) {
        setResetParity(CommonUtils.getInstance().getDecimalEquivalent(value));
    }

    public void setResetAC(int value1, int value2) {

        if (((value1 & 0b1111) + (value2 & 0b1111) > Constants.MAX_VALUE_FOUR_BITS) || (value1 & 0b1111) - (value2 &
                0b1111) < 0) {
            cpu.getPSW().setFlag(PSWFlag.AC);
        } else {
            cpu.getPSW().resetFlag(PSWFlag.AC);
        }
    }

    public void setResetOV(int value) {

        if (value > Constants.MAX_VALUE_SIGNED || value < Constants.MIN_VALUE_SIGNED) {
            cpu.getPSW().setFlag(PSWFlag.OV);
        } else {
            cpu.getPSW().resetFlag(PSWFlag.OV);
        }
    }

    public String getRegisterValue(String operand) {
        int registerNum = Integer.parseInt(operand.replace("R", ""));
        int currentBank = cpu.getPSW().getCurrentBank();
        return RAM.getInstance().getRB()[currentBank].getRegisterValue(registerNum);
    }

    public void setRegisterValue(String operand, String value) {
        int registerNum = Integer.parseInt(operand.replace("R", ""));
        int currentBank = cpu.getPSW().getCurrentBank();
        RAM.getInstance().getRB()[currentBank].setRegisterValue(registerNum, value);
    }

    public String getRegisterIndirectValue(String operand) {
        int registerNum = Integer.parseInt(operand.replace("@R", ""));
        int currentBank = cpu.getPSW().getCurrentBank();
        String registerValue = RAM.getInstance().getRB()[currentBank].getRegisterValue(registerNum);
        return RAM.getInstance().getValue(registerValue);
    }

    public void setRegisterIndirectValue(String operand, String value) {
        int registerNum = Integer.parseInt(operand.replace("@R", ""));
        int currentBank = cpu.getPSW().getCurrentBank();
        String registerValue = RAM.getInstance().getRB()[currentBank].getRegisterValue(registerNum);
        RAM.getInstance().setValue(registerValue, value);
    }
}
