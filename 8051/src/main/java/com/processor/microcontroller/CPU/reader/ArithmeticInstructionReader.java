package com.processor.microcontroller.CPU.reader;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.CPU.CPU;
import com.processor.microcontroller.CPU.PSWFlag;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;
import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.Operand;
import com.processor.microcontroller.RAM.RAM;

public class ArithmeticInstructionReader extends InstructionReader {

    private static final ArithmeticInstructionReader INSTANCE = new ArithmeticInstructionReader();
    private static final Logger LOGGER = Logger.getLogger(ArithmeticInstructionReader.class.getName());
    private CPU cpu;

    public static ArithmeticInstructionReader getInstance() {
        return INSTANCE;
    }

    private ArithmeticInstructionReader() {
        cpu = CPU.getInstance();
    }

    public void executeInstruction(Instruction instruction) {
        LOGGER.log(Level.FINER, "-->> executeInstructions() -> instruction: {0}", instruction);

        try {
            switch (instruction.getMnemonic()) {
                case ADD:
                    ADD(instruction.getOperands()[0], instruction.getOperands()[1]);
                    break;

                case ADDC:
                    ADDC(instruction.getOperands()[0], instruction.getOperands()[1]);
                    break;

                case SUBB:
                    SUBB(instruction.getOperands()[0], instruction.getOperands()[1]);
                    break;

                case INC:
                    INC(instruction.getOperands()[0]);
                    break;

                case DEC:
                    DEC(instruction.getOperands()[0]);
                    break;

                case MUL:
                    MUL(instruction.getOperands()[0]);
                    break;

                case DIV:
                    DIV(instruction.getOperands()[0]);
                    break;

                case DA:
                    DA(instruction.getOperands()[0]);
                    break;

                default:
                    throw new InstructionValidatorException("Invalid Mnemonic: " + instruction.getMnemonic().name() + " found for the Arithmetic instruction: " + instruction.toString());

            }
        } catch (InstructionValidatorException e) {
            throw e;
        }

        LOGGER.log(Level.FINER, "<<-- executeInstructions()");

    }

    private  String addOperands(String value1, String value2) {
        LOGGER.log(Level.FINER, "-->> addOperands() -> value1: " + value1 + ", value2: " + value2);

        int decimalVal1 = CommonUtils.getInstance().getDecimalEquivalent(value1);
        int decimalVal2 = CommonUtils.getInstance().getDecimalEquivalent(value2);
        int decimalResult = decimalVal1 + decimalVal2;
        CommonUtilsReader.getInstance().setResetCY(decimalResult);
        CommonUtilsReader.getInstance().setResetAC(decimalVal1, decimalVal2);
        CommonUtilsReader.getInstance().setResetOV(decimalResult);

        decimalResult = decimalResult > Constants.MAX_VALUE ? decimalResult - Constants.MAX_VALUE - 1: decimalResult;
        CommonUtilsReader.getInstance().setResetParity(decimalResult);

        LOGGER.log(Level.FINER, "<<-- addOperands()");
        return CommonUtils.getInstance().getHexadecimalEquivalent(decimalResult);

    }

    private void ADD(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ADD() -> operand 1: " + operand1 + ", operand 2: " + operand2);
        String result;

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                result = addOperands(cpu.getACC().getValue(), operand2.replaceAll("#", ""));

            } else if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                result = addOperands(cpu.getACC().getValue(), CommonUtilsReader.getInstance().getRegisterValue
                        (operand2));

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                result = addOperands(cpu.getACC().getValue(), RAM.getInstance().getValue(CommonUtils.getInstance()
                        .getHexadecimalEquivalent(operand2)));

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                result = addOperands(cpu.getACC().getValue(), CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            cpu.getACC().setData(result);

        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand1 + " found during ADD() operation");
        }

        LOGGER.log(Level.FINER, "<<-- ADD()");
    }

    private void ADDC(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ADDC() -> operand 1: " + operand1 + ", operand 2: " + operand2);

        cpu.getACC().setData(addOperands(cpu.getACC().getValue(), CommonUtils.getInstance().getHexadecimalEquivalent
                (cpu.getPSW().getFlagValue(PSWFlag
                .CY))));
        ADD(operand1, operand2);

        LOGGER.log(Level.FINER, "<<-- ADD()");
    }

    private String subOperands(String value1, String value2) {
        LOGGER.log(Level.FINER, "-->> subOperands() -> value1: " + value1 + ", value2: " + value2);

        int decimalVal1 = CommonUtils.getInstance().getDecimalEquivalent(value1);
        int decimalVal2 = CommonUtils.getInstance().getDecimalEquivalent(value2);
        int decimalResult = decimalVal1 - decimalVal2;

        CommonUtilsReader.getInstance().setResetCY(decimalResult);
        CommonUtilsReader.getInstance().setResetAC(decimalVal1, decimalVal2);
        CommonUtilsReader.getInstance().setResetOV(decimalResult);

        decimalResult = decimalResult < Constants.MIN_VALUE_SIGNED ? Constants.MAX_VALUE_SIGNED - (Constants.MIN_VALUE_SIGNED - decimalResult) + 1 : decimalResult;
        CommonUtilsReader.getInstance().setResetParity(decimalResult);

        LOGGER.log(Level.FINER, "<<-- addOperands()");
        return CommonUtils.getInstance().getHexadecimalEquivalent(decimalResult);

    }

    private void SUBB(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> SUBB() -> operand 1: " + operand1 + ", operand 2: " + operand2);

        String result;

        cpu.getACC().setData(subOperands(cpu.getACC().getValue(), Integer.toString(cpu.getPSW().getFlagValue(PSWFlag.CY))));

        if (Operand.ACCUMULATOR.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandImmediate(operand2)) {
                result = subOperands(cpu.getACC().getValue(), operand2.replaceAll("#", ""));

            } else if (CommonUtils.getInstance().isOperandRegister(operand2)) {
                result = subOperands(cpu.getACC().getValue(), CommonUtilsReader.getInstance().getRegisterValue(operand2));

            } else if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                result = subOperands(cpu.getACC().getValue(), RAM.getInstance().getValue(CommonUtils.getInstance()
                        .getHexadecimalEquivalent(operand2)));

            } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand2)) {
                result = subOperands(cpu.getACC().getValue(), CommonUtilsReader.getInstance().getRegisterIndirectValue(operand2));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");

            }
            cpu.getACC().setData(result);

        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand1 + " found during ADD operation");
        }

        LOGGER.log(Level.FINER, "<<-- SUBB()");

    }

    private void INC(String operand) {
        LOGGER.log(Level.FINER, "-->> INC() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            decimalVal++;
            decimalVal = decimalVal > Constants.MAX_VALUE ? decimalVal - Constants.MAX_VALUE - 1 : decimalVal;
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
            CommonUtilsReader.getInstance().setResetParity(decimalVal);

        } else if (CommonUtils.getInstance().isOperandRegister(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterValue(operand));
            decimalVal++;
            decimalVal = decimalVal > Constants.MAX_VALUE ? decimalVal - Constants.MAX_VALUE - 1 : decimalVal;
            CommonUtilsReader.getInstance().setRegisterValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(RAM.getInstance().getValue(operand));
            decimalVal++;
            RAM.getInstance().setValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterIndirectValue(operand));
            decimalVal++;
            CommonUtilsReader.getInstance().setRegisterIndirectValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- INC()");

    }

    private void DEC(String operand) {
        LOGGER.log(Level.FINER, "-->> DEC() -> operand: {0}", operand);

        if (Operand.ACCUMULATOR.getValue().equals(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
            decimalVal--;
            decimalVal = decimalVal < Constants.MIN_VALUE_SIGNED ? Constants.MAX_VALUE_SIGNED : decimalVal;
            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));
            CommonUtilsReader.getInstance().setResetParity(decimalVal);

        } else if (CommonUtils.getInstance().isOperandRegister(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterValue(operand));
            decimalVal--;
            decimalVal = decimalVal < Constants.MIN_VALUE_SIGNED ? Constants.MAX_VALUE_SIGNED : decimalVal;
            CommonUtilsReader.getInstance().setRegisterValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(RAM.getInstance().getValue(operand));
            decimalVal--;
            RAM.getInstance().setValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else if (CommonUtils.getInstance().isOperandRegisterIndirect(operand)) {
            int decimalVal = CommonUtils.getInstance().getDecimalEquivalent(CommonUtilsReader.getInstance().getRegisterIndirectValue(operand));
            decimalVal--;
            CommonUtilsReader.getInstance().setRegisterIndirectValue(operand, CommonUtils.getInstance().getHexadecimalEquivalent(decimalVal));

        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- DEC()");

    }

    private void MUL(String operand) {
        LOGGER.log(Level.FINER, "-->> MUL() -> operand: {0}", operand);

        if (Operand.AB.getValue().equals(operand)) {
            int mulResult = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue()) * CommonUtils
                    .getInstance().getDecimalEquivalent(cpu.getB().getValue());
            if (mulResult > Constants.MAX_VALUE) {
                cpu.getPSW().setFlag(PSWFlag.OV);
            }

            cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(mulResult & 0b11111111));
            cpu.getB().setValue(CommonUtils.getInstance().getHexadecimalEquivalent((mulResult >> Constants
                    .BITS_PER_REGISTER) & 0b11111111));
            cpu.getPSW().resetFlag(PSWFlag.CY);
            CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());

        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand + " found during MUL operation");
        }

        LOGGER.log(Level.FINER, "<<-- MUL()");

    }

    private void DIV(String operand) {
        LOGGER.log(Level.FINER, "-->> DIV() -> operand: {0}", operand);

        if (Operand.AB.getValue().equals(operand)) {
            if (cpu.getB().getValue().equals("00")) {
                cpu.getPSW().setFlag(PSWFlag.OV);
                cpu.getACC().setData("00");
                cpu.getB().setValue("00");
                cpu.getPSW().resetFlag(PSWFlag.P);

            } else {
                int dividend = CommonUtils.getInstance().getDecimalEquivalent(cpu.getACC().getValue());
                int divisor = CommonUtils.getInstance().getDecimalEquivalent(cpu.getB().getValue());

                if (divisor == 0) {
                    cpu.getPSW().setFlag(PSWFlag.OV);

                } else {
                    cpu.getACC().setData(CommonUtils.getInstance().getHexadecimalEquivalent(dividend / divisor));
                    cpu.getB().setValue(CommonUtils.getInstance().getHexadecimalEquivalent(dividend % divisor));
                    cpu.getPSW().resetFlag(PSWFlag.CY);
                    cpu.getPSW().resetFlag(PSWFlag.OV);
                    CommonUtilsReader.getInstance().setResetParity(cpu.getACC().getValue());
                }
            }
        } else {
            throw new InstructionValidatorException("Invalid operand: " + operand + " found during DIV operation");
        }

        LOGGER.log(Level.FINER, "<<-- DIV()");

    }

    private void DA(String operand) {
        LOGGER.log(Level.INFO, "This operation is currently not supported by MC8051");

    }
}
