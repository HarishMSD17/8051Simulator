package com.processor.microcontroller.CPU.reader;

import java.util.logging.Logger;

import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;

public class BranchInstructionReader extends InstructionReader {

    private static final BranchInstructionReader INSTANCE = new BranchInstructionReader();
    private static final Logger LOGGER = Logger.getLogger(BranchInstructionReader.class.getName());

    public static BranchInstructionReader getInstance() {
        return INSTANCE;
    }

    private BranchInstructionReader() {
    }

    public void executeInstruction(Instruction instruction) {
        throw new InstructionValidatorException("Branch instruction: " + instruction.toString() + " is currently not supported by MC8051");
    }
}
