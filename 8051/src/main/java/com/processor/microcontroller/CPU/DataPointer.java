package com.processor.microcontroller.CPU;

import java.util.HashMap;
import java.util.Map;

import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Constants;

public class DataPointer {
    private static final DataPointer INSTANCE = new DataPointer();
    private Map<String, String> data;

    private DataPointer() {
        data = new HashMap<String, String>();
        data.put(Constants.DPL_ADDRESS, Constants.DPL_START_VALUE);
        data.put(Constants.DPH_ADDRESS, Constants.DPH_START_VALUE);
    }

    public static DataPointer getInstance() {
        return INSTANCE;
    }

    public String getValue() {
        return data.get(Constants.DPH_ADDRESS).replace("H", "") + data.get(Constants.DPL_ADDRESS);
    }

    public void setValue(String value) {
        String hexValue4bytes = CommonUtils.getInstance().getHexadecimalEquivalent(value, Constants.HEXBITS_PER_DPTR);
        setDPHValue(hexValue4bytes.substring(0, 2) + "H");
        setDPLValue(hexValue4bytes.substring(2, 4) + "H");
    }

    public String getDPLValue() {
        return data.get(Constants.DPL_ADDRESS);
    }

    public void setDPLValue(String dplValue) {
        data.put(Constants.DPL_ADDRESS, dplValue);
    }

    public String getDPHValue() {
        return data.get(Constants.DPH_ADDRESS);
    }

    public void setDPHValue(String dphValue) {
        data.put(Constants.DPH_ADDRESS, dphValue);
    }

    /*
 * (non-Javadoc)
 * @see java.lang.Object#toString()
 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName())
                .append(" {\n\tdata: ").append(data)
                .append("\n}");
        return builder.toString();
    }
}
