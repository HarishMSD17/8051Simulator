package com.processor.microcontroller.CPU;

public enum PSWFlag {
    P(0), UD(1), OV(2), RS0(3), RS1(4), F0(5), AC(6), CY(7);

    private int flagPosition;

    PSWFlag(int flagPosition) {
        this.flagPosition = flagPosition;
    }
    /**
     * @return the flagPosition
     */
    public int getFlagPosition() {
        return flagPosition;
    }
}
