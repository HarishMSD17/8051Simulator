package com.processor.microcontroller.CPU.reader;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.processor.microcontroller.CPU.CPU;
import com.processor.microcontroller.CPU.PSWFlag;
import com.processor.microcontroller.CommonUtils;
import com.processor.microcontroller.Instruction;
import com.processor.microcontroller.InstructionValidatorException;
import com.processor.microcontroller.Operand;
import com.processor.microcontroller.RAM.RAM;

public class BooleanInstructionReader extends InstructionReader {

    private static final BooleanInstructionReader INSTANCE = new BooleanInstructionReader();
    private static final Logger LOGGER = Logger.getLogger(BooleanInstructionReader.class.getName());
    private CPU cpu;
    private RAM ram;

    public static BooleanInstructionReader getInstance() {
        return INSTANCE;
    }

    private BooleanInstructionReader() {
        cpu = CPU.getInstance();
        ram = RAM.getInstance();
    }

    public void executeInstruction(Instruction instruction) {
        LOGGER.log(Level.FINER, "-->> executeInstructions() -> instruction: {0}", instruction);

        switch (instruction.getMnemonic()) {
            case CLR:
                CLR(instruction.getOperands()[0]);
                break;

            case SETB:
                SETB(instruction.getOperands()[0]);
                break;

            case CPL:
                CPL(instruction.getOperands()[0]);
                break;

            case ANL:
                ANL(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case ORL:
                ORL(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            case MOV:
                MOV(instruction.getOperands()[0], instruction.getOperands()[1]);
                break;

            default:
                throw new InstructionValidatorException("Invalid Mnemonic: " + instruction.getMnemonic().name() + " " +
                        "found for the Boolean instruction: " + instruction.toString());
        }

        LOGGER.log(Level.FINER, "<<-- executeInstructions()");
    }

    void CLR(String operand) {
        LOGGER.log(Level.FINER, "-->> CLR() -> operand: {0}", operand);

        if (Operand.C.getValue().equals(operand)) {
            cpu.getPSW().resetFlag(PSWFlag.CY);

        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            ram.getBitAddrRAM().setBitData(operand, 0);
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- CLR()");

    }

    void SETB(String operand) {
        LOGGER.log(Level.FINER, "-->> SETB() -> operand: {0}", operand);

        if (Operand.C.getValue().equals(operand)) {
            cpu.getPSW().setFlag(PSWFlag.CY);

        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            ram.getBitAddrRAM().setBitData(operand, 1);
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- SETB()");

    }

    void CPL(String operand) {
        LOGGER.log(Level.FINER, "-->> CPL() -> operand: {0}", operand);

        if (Operand.C.getValue().equals(operand)) {
            cpu.getPSW().toggleFlag(PSWFlag.CY);

        } else if (CommonUtils.getInstance().isOperandDirect(operand)) {
            ram.getBitAddrRAM().toggleBitData(CommonUtils.getInstance().getHexadecimalEquivalent(operand));
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand + " found");
        }

        LOGGER.log(Level.FINER, "<<-- CPL()");

    }

    void ANL(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ANL() -> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.C.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                cpu.getPSW().setFlagValue(PSWFlag.CY, cpu.getPSW().getFlag(PSWFlag.CY) && ram.getBitAddrRAM()
                        .getBitDataBool(operand2));
            } else if (CommonUtils.getInstance().isOperandComplementDirect(operand2)) {
                cpu.getPSW().setFlagValue(PSWFlag.CY, cpu.getPSW().getFlag(PSWFlag.CY) && !ram.getBitAddrRAM()
                        .getBitDataBool(operand2));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }
        LOGGER.log(Level.FINER, "<<-- ANL()");

    }

    void ORL(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> ORL() -> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.C.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                cpu.getPSW().setFlagValue(PSWFlag.CY, cpu.getPSW().getFlag(PSWFlag.CY) || ram.getBitAddrRAM()
                        .getBitDataBool(operand2));
            } else if (CommonUtils.getInstance().isOperandComplementDirect(operand2)) {
                cpu.getPSW().setFlagValue(PSWFlag.CY, cpu.getPSW().getFlag(PSWFlag.CY) || !ram.getBitAddrRAM()
                        .getBitDataBool(operand2));

            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }

        LOGGER.log(Level.FINER, "<<-- ORL()");

    }

    void MOV(String operand1, String operand2) {
        LOGGER.log(Level.FINER, "-->> MOV() -> operand1: " + operand1 + ", operand2: " + operand2);

        if (Operand.C.getValue().equals(operand1)) {
            if (CommonUtils.getInstance().isOperandDirect(operand2)) {
                cpu.getPSW().setFlagValue(PSWFlag.CY, ram.getBitAddrRAM().getBitDataBool(operand2));
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else if (CommonUtils.getInstance().isOperandDirect(operand1)) {
            if (Operand.C.getValue().equals(operand2)) {
                ram.getBitAddrRAM().setBitDataBool(operand1, cpu.getPSW().getFlag(PSWFlag.CY));
            } else {
                throw new InstructionValidatorException("Invalid Operand: " + operand2 + " found");
            }
        } else {
            throw new InstructionValidatorException("Invalid Operand: " + operand1 + " found");
        }

        LOGGER.log(Level.FINER, "<<-- MOV()");

    }

}
