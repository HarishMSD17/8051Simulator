package com.processor.microcontroller;

public enum Mnemonic {

    ORG, NOP, AJMP, LJMP, RR, INC, JBC, ACALL, LCALL, RRC, DEC, JB, RET, RL, ADD, JNB, RETI, RLC, ADDC, JC,
    ORL, JNC, ANL, JZ, XRL, JNZ, JMP, MOV, SJMP, MOVC, DIV, SUBB, MUL, CPL, CJNE, PUSH, SWAP, XCH, POP,
    SETB, DA, DJNZ, XCHD, MOVX, CLR, END;
}
