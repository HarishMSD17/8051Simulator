package com.processor.microcontroller;

import java.util.Arrays;

public class Instruction {

    private Mnemonic mnemonic;
    private String[] operands;

    public Instruction() {

    }

    public Instruction(Mnemonic mnemonic, String[] operands) {
        this.mnemonic = mnemonic;
        this.operands = operands;
    }

    /**
     * @return the mnemonic
     */
    public Mnemonic getMnemonic() {
        return mnemonic;
    }

    /**
     * @param mnemonic
     *         the mnemonic to set
     */
    public void setMnemonic(Mnemonic mnemonic) {
        this.mnemonic = mnemonic;
    }

    /**
     * @return the operands
     */
    public String[] getOperands() {
        return operands;
    }

    /**
     * @param operands
     *         the operands to set
     */
    public void setOperands(String[] operands) {
        this.operands = operands;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Instruction that = (Instruction) o;

        if (mnemonic != that.mnemonic) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(operands, that.operands);
    }

    @Override
    public int hashCode() {
        int result = mnemonic.hashCode();
        result = 31 * result + Arrays.hashCode(operands);
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getMnemonic())
                .append(" ")
                .append(Arrays.toString(getOperands()).replaceAll("\\[|\\]", ""));
        return builder.toString();
    }
}
