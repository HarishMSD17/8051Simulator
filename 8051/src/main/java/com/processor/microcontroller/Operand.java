package com.processor.microcontroller;

public enum Operand {

    PAGE0("page0"), PAGE1("page1"), PAGE2("page2"), PAGE3("page3"), PAGE4("page4"), PAGE5("page5"), PAGE6("page6"), PAGE7("page7"),
    ADDR16("addr16"), ACCUMULATOR("A"), B("B"), C("C"), AB("AB"), DIRECT("direct"), _R0("@R0"), _R1("@R1"), R0("RO"),
    R1("R1"), R2("R2"),
    R3("R3"), R4("R4"), R5("R5"), R6("R6"), R7("R7"), BIT("bit"), OFFSET("offset"), IMMEDIATE("immed"), A_PC("@A+PC"), A_DPTR("@A+DPTR"),
    _BIT("/bit"), _DPTR("@DPTR"), DPTR("DPTR"), _DIRECT("/direct");

    private String value;

    Operand(String value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    public static boolean isValuePresent(String value) {

        boolean isPresent = false;

        for (Operand operand : Operand.values()) {
            if (operand.value.equals(value)) {
                isPresent = true;
                break;
            }
        }

        return isPresent;
    }
}
