package com.processor.microcontroller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommandLineParser {

    private static final Logger LOGGER = Logger.getLogger(CommandLineParser.class.getName());
    private static final CommandLineParser INSTANCE = new CommandLineParser();

    private CommandLineParser() {

    }

    public static CommandLineParser getInstance() {
        return INSTANCE;
    }

    public CommandLine parseCommandLineOptions(String[] args) throws IOException {

        Options options = new Options();

        Option instructionsFilePath = new Option("p", Constants.FILEPATH, true, "Instructions File Path");
        instructionsFilePath.setRequired(true);
        options.addOption(instructionsFilePath);

        Option resultFilePath = new Option("d", Constants.DESTINATION, true, "Destination File Path");
        resultFilePath.setRequired(true);
        options.addOption(resultFilePath);

        Option verbose = new Option("v", Constants.VERBOSE, false, "To print all debug logs (optional)");
        options.addOption(verbose);

        org.apache.commons.cli.CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            validateInputs(cmd);
        } catch (ParseException e) {
            cmd = null;
            System.out.println(e.getMessage());
            formatter.printHelp("java -jar mc8051-1.0.jar [OPTION]...", options);
            System.exit(1);
        }

        return cmd;
    }

    private void validateInputs(CommandLine cmd) throws FileNotFoundException {

        String instructionsFilePath = cmd.getOptionValue(Constants.FILEPATH);
        if (! new File(instructionsFilePath).exists()) {
            throw new FileNotFoundException("Input File path " + instructionsFilePath + " not found");
        }

        String destinationPath = cmd.getOptionValue(Constants.DESTINATION);
        if (! new File(destinationPath).exists()) {
            throw new FileNotFoundException("Desination path " + destinationPath + " not found");
        }
    }

    public Map<String,String> getInputsMap(CommandLine cmd) {

        Map<String, String> inputs = new HashMap<String, String>();

        inputs.put(Constants.FILEPATH, cmd.getOptionValue(Constants.FILEPATH));
        String destination = cmd.getOptionValue(Constants.DESTINATION);
        if (!destination.endsWith("/")) {
            destination += "/";
        }
        inputs.put(Constants.DESTINATION, destination);

        return inputs;
    }
}
