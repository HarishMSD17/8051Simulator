package com.processor.microcontroller;

public class Constants {

    public final static String FILEPATH = "filepath";
    public final static String OUTPUT_FILENAME = "Result.txt";
    public static final String DESTINATION = "destination";
    public static final String INSTRUCTIONSET_FILEPATH = "./resources/Opcodes.txt";
    public static final String START_ADDRESS = "0000";
    public static final String LOG_DIR = "./logs/";
    public static final String FILEHANDLER_LOGPATH = LOG_DIR + "8051.log";
    public static final String VERBOSE = "verbose";
    public static final int MAX_VALUE = 255;
    public static final int MAX_VALUE_HEX = 0xFF;
    public static final int MAX_VALUE_SIGNED = 127;
    public static final int MAX_VALUE_FOUR_BITS = 15;
    public static final String ACCUMULATOR_ADDRESS = "EO";
    public static final String STACKPOINTER_ADDRESS = "81";
    public static final String PSW_ADDRESS = "D0";
    public static final int PSW_SIZE = 8;
    public static final String BREG_ADDRESS = "F0";
    public static final String BITADRESSABLE_START_ADDRESS = "20";
    public static final String BITADRESSABLE_END_ADDRESS = "2F";
    public static final String SCRATCHPAD_START_ADDRESS = "30";
    public static final String SCRATCHPAD_END_ADDRESS = "7F";
    public static final int REGISTERS_PER_BANK = 8;
    public static final int HEXBITS_PER_PC = 4;
    public static final int HEXBITS_PER_REGISTER = 2;
    public static final String CARRY_FLAG = "C";
    public static final String ACCUMULATOR = "A";
    public static final int ACCUMULATOR_SIZE_HEX = 2;
    public static final String STACKPOINTER_INITIAL_VALUE = "07H";
    public static final String ACCUMULATOR_INITIAL_VALUE = "00H";
    public static final String BREG_START_VALUE = "00H";
    public static final String DPL_ADDRESS = "82";
    public static final String DPL_START_VALUE = "00H";
    public static final String DPH_ADDRESS = "83";
    public static final String DPH_START_VALUE = "00H";
    public static final String PC_START_VALUE = "0000";
    public static final int MIN_VALUE_SIGNED = -128;
    public static final short BITS_PER_REGISTER = 8;
    public static final int HEXBITS_PER_DPTR = 4;
    public static final int TOTAL_REGISTER_BANKS = 4;
}
