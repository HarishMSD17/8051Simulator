package com.processor.microcontroller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InstructionReader {

    private static final Logger LOGGER = Logger.getLogger(InstructionReader.class.getName());
    private static final InstructionReader INSTANCE = new InstructionReader();

    private InstructionReader() {

    }

    public static InstructionReader getInstance() {
        return INSTANCE;
    }

    public List<Instruction> readInstructions(String instructionsFilePath) throws IOException {

        LOGGER.log(Level.FINER, "-->> readInstructions() -> instructionsFilePath: {0}", instructionsFilePath);

        List<Instruction> instructions = new ArrayList<Instruction>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(instructionsFilePath));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                Instruction instruction = getInstruction(strLine);
                if (instruction != null) {
                    instructions.add(instruction);

                    if (instruction.getMnemonic() == Mnemonic.END) {
                        break;
                    }
                }
            }

        } catch (IOException e) {
            throw e;
        } catch (InstructionValidatorException e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
        }

        LOGGER.log(Level.FINER, "<<-- readInstructions()");

        return instructions;

    }

    public Instruction getInstruction(String strLine) {

        LOGGER.log(Level.FINER, "-->> getInstruction() -> strLine: {0}", strLine);

        if (strLine.length() == 0) {
            return null;
        }

        String[] instruction = strLine.split(" ", 2);
        Mnemonic mnemonicEnum = getMnemonic(instruction[0].trim());

        String[] operands = null;
        if (instruction.length > 1) {
            operands = getOperands(instruction[1]);
        }

        LOGGER.log(Level.FINER, "<<-- getInstruction()");

        return new Instruction(mnemonicEnum, operands);
    }

    private String[] getOperands(String operandStr) {

        LOGGER.log(Level.FINER, "-->> getOperands() -> operandStr: {0}", operandStr);

        String[] operands = operandStr.split(",");

        for (int i = 0; i < operands.length; i++) {
            operands[i] = operands[i].trim();
        }

        LOGGER.log(Level.FINER, "<<-- getMnemonic()");

        return operands;
    }

    private Mnemonic getMnemonic(String mnemonic) {

        LOGGER.log(Level.FINER, "Starting getMnemonic() for mnemonic: {0}", mnemonic);

        Mnemonic mnemonicEnum;

        try {
            mnemonicEnum = Mnemonic.valueOf(mnemonic);
        } catch (IllegalArgumentException e) {
            throw new InstructionValidatorException("Invalid mnemonic for the string " + mnemonic, e);
        }

        LOGGER.log(Level.FINER, "<<-- getMnemonic()");

        return mnemonicEnum;
    }
}
